import React, { useState } from "react";
import { useColorMode } from "theme-ui";

const ModeToggle = ({ Component, pageProps }) => {
  const [colorMode, setColorMode] = useColorMode();
  const darkEmoji = "🌛";
  const lightEmoji = "🌞";
  const [emoji, setEmoji] = useState(darkEmoji);
  const toggle = () => {
    setColorMode(colorMode === "default" ? "dark" : "default");
    setEmoji(emoji === lightEmoji ? darkEmoji : lightEmoji);
  };
  return (
    <button
      style={{ border: "none", background: "inherit", fontSize: "2rem" }}
      onClick={(e) => toggle()}
    >
      {emoji}
    </button>
  );
};

export default ModeToggle;
